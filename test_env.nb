(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     25366,        542]
NotebookOptionsPosition[     24240,        514]
NotebookOutlinePosition[     24728,        532]
CellTagsIndexPosition[     24685,        529]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"Quit", "[", "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.829192341711194*^9, 3.829192345870385*^9}, {
   3.829193892359024*^9, 3.829193895672892*^9}, {3.829194543305023*^9, 
   3.829194546040998*^9}, 3.829313564891275*^9, 3.829898562383398*^9, {
   3.8298991375887213`*^9, 3.8298991402557364`*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"c44f47db-f7c4-455a-9dd1-e84dd032c2c4"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"Setting", " ", "directory", " ", "di", " ", "lavoro"}], "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"AppendTo", "[", 
     RowBox[{"$Path", ",", " ", 
      RowBox[{"NotebookDirectory", "[", "]"}]}], "]"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"<<", "ui_components.wl"}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"<<", "pm_library.wl"}], ";"}]}]}]], "Input",
 InitializationCell->True,
 CellChangeTimes->{{3.8291923474878273`*^9, 3.8291923909697533`*^9}, 
   3.829193237117037*^9, {3.82919465106369*^9, 3.829194665263527*^9}, {
   3.8291954495610533`*^9, 3.829195473236579*^9}, {3.82941228943139*^9, 
   3.82941229503423*^9}, {3.82965444161801*^9, 3.829654465226101*^9}, {
   3.829654575538582*^9, 3.8296545791162777`*^9}, {3.829794313096546*^9, 
   3.829794313648077*^9}, {3.829897762282344*^9, 3.8298977980874557`*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"4a9f226c-143e-42c8-8613-180a147a3e9e"],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.829632993574703*^9, 
  3.829632997660048*^9}},ExpressionUUID->"fb3adc08-6d92-4494-b297-\
79ebc8875880"],

Cell[CellGroupData[{

Cell[BoxData["wipEsUniversale"], "Input",
 CellChangeTimes->{{3.829633001292671*^9, 3.829633002244013*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"fd6c733d-0a84-4210-8cf9-ea9d17e23a3b"],

Cell[BoxData[
 DynamicModuleBox[{}, 
  TagBox[GridBox[{
     {"\<\"Numero medio di task conclusi per ogni persona in un mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`armp], String,
       FieldHint->"6,2,3"]},
     {"\<\"Numero di persone assegnate alla colonna che e` andata piu` lenta:\
\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`psc], Number,
       FieldHint->"3"]},
     {"\<\"Risultato:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`solution], String,
       FieldHint->"2, 5, 3"]},
     {
      StyleBox[
       DynamicBox[ToBoxes[UiComponents`Private`systemResponse, StandardForm],
        ImageSizeCache->{273., {3., 10.}}],
       StripOnInput->False,
       FontColor->GrayLevel[0, 0]], ""},
     {
      InterpretationBox[GridBox[{
         {
          ButtonBox["\<\"Genera Esercizio\"\>",
           Appearance->{"AbuttingRight"},
           ButtonFunction:>(UiComponents`Private`armp = StringRiffle[
               Table[
                RandomInteger[{1, 10}], {3}], ","]; 
            UiComponents`Private`psc = RandomInteger[{1, 5}]; Null),
           Evaluator->Automatic,
           Method->"Preemptive"], 
          ButtonBox["\<\"Verifica Risultato\"\>",
           Appearance->{"AbuttingLeftRight"},
           ButtonFunction:>If[
             And[
              And[
               ValueQ[UiComponents`Private`armp], 
               ValueQ[UiComponents`Private`psc], 
               ValueQ[UiComponents`Private`solution]], 
              Not[
               Or[
                TrueQ[UiComponents`Private`armp == ""], 
                TrueQ[UiComponents`Private`psc == Null], 
                TrueQ[UiComponents`Private`solution == ""]]]], 
             UiComponents`Private`calcResult; 
             If[StringDelete[UiComponents`Private`solution, " "] == 
               UiComponents`Private`wipResult, 
               UiComponents`Private`systemResponse = 
               Style["La tua risposta e` corretta!", FontColor -> Green], 
               UiComponents`Private`systemResponse = 
               Style["La risposta e` errata. Riprova!", FontColor -> Red]], 
             UiComponents`Private`systemResponse = 
             Style["Uno dei campi di input e` vuoto", FontColor -> Red]],
           Evaluator->Automatic,
           Method->"Preemptive"], 
          ButtonBox["\<\"Mostra Soluzione\"\>",
           Appearance->{"AbuttingLeftRight"},
           ButtonFunction:>If[
             And[
              And[
               ValueQ[UiComponents`Private`armp], 
               ValueQ[UiComponents`Private`psc]], 
              Not[
               Or[
                TrueQ[UiComponents`Private`armp == ""], 
                TrueQ[UiComponents`Private`psc == Null]]]], 
             UiComponents`Private`calcResult; 
             UiComponents`Private`solution = UiComponents`Private`wipResult, 
             UiComponents`Private`systemResponse = 
             Style["Uno dei campi di input e` vuoto", FontColor -> Red]],
           Evaluator->Automatic,
           Method->"Preemptive"], 
          ButtonBox["\<\"Pulisci Soluzione\"\>",
           Appearance->{"AbuttingLeft"},
           
           ButtonFunction:>(Clear[UiComponents`Private`solution]; 
            UiComponents`Private`systemResponse = 
             Style["", FontColor -> Transparent]),
           Evaluator->Automatic,
           Method->"Preemptive"]}
        },
        AutoDelete->False,
        GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.]}, 
            Offset[0.27999999999999997`]}, "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}}],
       ButtonBar[{
        "Genera Esercizio" :> (UiComponents`Private`armp = StringRiffle[
             Table[
              RandomInteger[{1, 10}], {3}], ","]; 
          UiComponents`Private`psc = RandomInteger[{1, 5}]; Null), 
         "Verifica Risultato" :> If[
           And[
            And[
             ValueQ[UiComponents`Private`armp], 
             ValueQ[UiComponents`Private`psc], 
             ValueQ[UiComponents`Private`solution]], 
            Not[
             Or[
              TrueQ[UiComponents`Private`armp == ""], 
              TrueQ[UiComponents`Private`psc == Null], 
              TrueQ[UiComponents`Private`solution == ""]]]], 
           UiComponents`Private`calcResult; 
           If[StringDelete[UiComponents`Private`solution, " "] == 
             UiComponents`Private`wipResult, 
             UiComponents`Private`systemResponse = 
             Style["La tua risposta e` corretta!", FontColor -> Green], 
             UiComponents`Private`systemResponse = 
             Style["La risposta e` errata. Riprova!", FontColor -> Red]], 
           UiComponents`Private`systemResponse = 
           Style["Uno dei campi di input e` vuoto", FontColor -> Red]], 
         "Mostra Soluzione" :> If[
           And[
            And[
             ValueQ[UiComponents`Private`armp], 
             ValueQ[UiComponents`Private`psc]], 
            Not[
             Or[
              TrueQ[UiComponents`Private`armp == ""], 
              TrueQ[UiComponents`Private`psc == Null]]]], 
           UiComponents`Private`calcResult; 
           UiComponents`Private`solution = UiComponents`Private`wipResult, 
           UiComponents`Private`systemResponse = 
           Style["Uno dei campi di input e` vuoto", FontColor -> Red]], 
         "Pulisci Soluzione" :> (Clear[UiComponents`Private`solution]; 
          UiComponents`Private`systemResponse = 
           Style["", FontColor -> Transparent])}, Appearance -> {Automatic}]],
       ""}
    },
    AutoDelete->False,
    GridBoxAlignment->{"Columns" -> {Left, Left}, "Rows" -> {Left, Left}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Grid"],
  DynamicModuleValues:>{}]], "Output",
 CellChangeTimes->{
  3.829633002676221*^9, 3.82963311149656*^9, 3.829633211005883*^9, 
   3.829633255386382*^9, 3.829633491899271*^9, 3.829654327163678*^9, 
   3.829654427213539*^9, 3.829654501694002*^9, {3.829654554962123*^9, 
   3.82965456626481*^9}, 3.829794146563291*^9, 3.829794270543621*^9, 
   3.8297946183671007`*^9, 3.829794764126026*^9, {3.829892315089683*^9, 
   3.8298923389963913`*^9}, 3.829977830622757*^9, 3.830227050395173*^9, {
   3.830227229678261*^9, 3.8302272344766693`*^9}, 3.830227293011662*^9, 
   3.830227413508485*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"11c389b5-a649-47fd-b4d0-62928cc3aab7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["expectedCompletionDateEsUniversale"], "Code",
 InitializationCell->False,
 CellChangeTimes->{{3.829889124762398*^9, 3.82988918163306*^9}, 
   3.829889414829445*^9, {3.829889515697379*^9, 3.82988952262302*^9}, {
   3.829889601782565*^9, 3.829890193374995*^9}, {3.8298904855196753`*^9, 
   3.829890568848794*^9}, {3.829890626317504*^9, 3.829890629530991*^9}, {
   3.829890706511025*^9, 3.829890719554935*^9}, {3.8298907558929157`*^9, 
   3.8298907673625603`*^9}, {3.829891299964615*^9, 3.829891464471304*^9}, {
   3.829891529061198*^9, 3.829891649693057*^9}, {3.829891679960428*^9, 
   3.8298917507268867`*^9}, {3.829891951442904*^9, 3.829892005416205*^9}, {
   3.8298922934781303`*^9, 3.829892297044281*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"5c6a4752-8dcf-4890-ac63-c0dab3d15098"],

Cell[BoxData[
 DynamicModuleBox[{}, 
  TagBox[GridBox[{
     {"\<\"Stima dei task in sospeso a inizio mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`etpb], Number,
       FieldHint->"74"]},
     {"\<\"Task attivi a inizio mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`atb], Number,
       FieldHint->"15"]},
     {"\<\"Task completati a inizio mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`dtb], Number,
       FieldHint->"2"]},
     {"\<\"Stima dei task in sospeso a fine mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`etpe], Number,
       FieldHint->"55"]},
     {"\<\"Task ancora attivi a fine mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`ate], Number,
       FieldHint->"16"]},
     {"\<\"Task completati a fine mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`dte], Number,
       FieldHint->"25"]},
     {"\<\"Giorni nel mese:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`days], Number,
       FieldHint->"29"]},
     {"\<\"Task completion rate:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`tcr], Number,
       FieldHint->"0.79"]},
     {"\<\"Task add rate:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`tar], Number,
       FieldHint->"0.17"]},
     {"\<\"Current task estimate:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`cte], Number,
       FieldHint->"71"]},
     {"\<\"Giorni rimanenti al completamento:\"\>", 
      InputFieldBox[Dynamic[UiComponents`Private`solution], Number,
       FieldHint->"114"]},
     {
      StyleBox[
       DynamicBox[ToBoxes[UiComponents`Private`systemResponse, StandardForm],
        ImageSizeCache->{273., {3., 10.}}],
       StripOnInput->False,
       FontColor->GrayLevel[0, 0]], ""},
     {
      InterpretationBox[GridBox[{
         {
          ButtonBox["\<\"Genera Esercizio\"\>",
           Appearance->{"AbuttingRight"},
           
           ButtonFunction:>(UiComponents`Private`tarResult = 1; 
            UiComponents`Private`tcrResult = 0; While[
              Or[
              UiComponents`Private`tarResult > UiComponents`Private`tcrResult,
                UiComponents`Private`tarResult == 
               UiComponents`Private`tcrResult], 
              UiComponents`Private`etpb = RandomInteger[{10, 30}]; 
              UiComponents`Private`atb = RandomInteger[{10, 30}]; 
              UiComponents`Private`dtb = RandomInteger[{0, 5}]; 
              UiComponents`Private`etpe = RandomInteger[{10, 30}]; 
              UiComponents`Private`ate = RandomInteger[{10, 30}]; 
              UiComponents`Private`dte = RandomInteger[{10, 30}]; 
              UiComponents`Private`dtb = RandomInteger[{0, 5}]; 
              UiComponents`Private`dte = RandomInteger[{10, 30}]; 
              UiComponents`Private`days = RandomInteger[{27, 30}]; 
              UiComponents`Private`calcResult; Null]; Null),
           Evaluator->Automatic,
           Method->"Preemptive"], 
          ButtonBox["\<\"Verifica Risultato\"\>",
           Appearance->{"AbuttingLeftRight"},
           ButtonFunction:>If[
             And[
              And[
               ValueQ[UiComponents`Private`etpb], 
               ValueQ[UiComponents`Private`atb], 
               ValueQ[UiComponents`Private`dtb], 
               ValueQ[UiComponents`Private`etpe], 
               ValueQ[UiComponents`Private`ate], 
               ValueQ[UiComponents`Private`dte], 
               ValueQ[UiComponents`Private`days], 
               ValueQ[UiComponents`Private`tcr], 
               ValueQ[UiComponents`Private`solution], 
               ValueQ[UiComponents`Private`tar], 
               ValueQ[UiComponents`Private`cte]], 
              Not[
               Or[
                TrueQ[UiComponents`Private`etpb == Null], 
                TrueQ[UiComponents`Private`atb == Null], 
                TrueQ[UiComponents`Private`dtb == Null], 
                TrueQ[UiComponents`Private`etpe == Null], 
                TrueQ[UiComponents`Private`ate == Null], 
                TrueQ[UiComponents`Private`dte == Null], 
                TrueQ[UiComponents`Private`days == Null], 
                TrueQ[UiComponents`Private`tar == Null], 
                TrueQ[UiComponents`Private`cte == Null], 
                TrueQ[UiComponents`Private`tcr == Null], 
                TrueQ[UiComponents`Private`solution == Null]]]], 
             UiComponents`Private`calcResult; If[
               And[
               UiComponents`Private`tar == UiComponents`Private`tarResult, 
                UiComponents`Private`tcr == UiComponents`Private`tcrResult, 
                UiComponents`Private`cte == UiComponents`Private`cteResult, 
                UiComponents`Private`solution == 
                UiComponents`Private`ecDateResult], 
               UiComponents`Private`systemResponse = 
               Style["La tua risposta e` corretta!", FontColor -> Green], 
               UiComponents`Private`systemResponse = 
               Style["La risposta e` errata. Riprova!", FontColor -> Red]], 
             UiComponents`Private`systemResponse = 
             Style["Uno dei campi di input e` vuoto", FontColor -> Red]],
           Evaluator->Automatic,
           Method->"Preemptive"], 
          ButtonBox["\<\"Mostra Soluzione\"\>",
           Appearance->{"AbuttingLeftRight"},
           ButtonFunction:>(If[
              And[
               And[
                ValueQ[UiComponents`Private`etpb], 
                ValueQ[UiComponents`Private`atb], 
                ValueQ[UiComponents`Private`dtb], 
                ValueQ[UiComponents`Private`etpe], 
                ValueQ[UiComponents`Private`ate], 
                ValueQ[UiComponents`Private`dte], 
                ValueQ[UiComponents`Private`days]], 
               Not[
                Or[
                 TrueQ[UiComponents`Private`etpb == Null], 
                 TrueQ[UiComponents`Private`atb == Null], 
                 TrueQ[UiComponents`Private`dtb == Null], 
                 TrueQ[UiComponents`Private`etpe == Null], 
                 TrueQ[UiComponents`Private`ate == Null], 
                 TrueQ[UiComponents`Private`dte == Null], 
                 TrueQ[UiComponents`Private`days == Null]]]], 
              UiComponents`Private`calcResult; 
              UiComponents`Private`cte = UiComponents`Private`cteResult; 
              UiComponents`Private`tar = UiComponents`Private`tarResult; 
              UiComponents`Private`tcr = UiComponents`Private`tcrResult; 
              UiComponents`Private`solution = 
               UiComponents`Private`ecDateResult; Null, 
              UiComponents`Private`systemResponse = 
              Style["Uno dei campi di input e` vuoto", FontColor -> Red]]; 
            Null),
           Evaluator->Automatic,
           Method->"Preemptive"], 
          ButtonBox["\<\"Pulisci Soluzione\"\>",
           Appearance->{"AbuttingLeft"},
           
           ButtonFunction:>(Clear[UiComponents`Private`cte]; 
            Clear[UiComponents`Private`tar]; Clear[UiComponents`Private`tcr]; 
            Clear[UiComponents`Private`solution]; 
            UiComponents`Private`systemResponse = 
             Style["", FontColor -> Transparent]),
           Evaluator->Automatic,
           Method->"Preemptive"]}
        },
        AutoDelete->False,
        GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
        GridBoxSpacings->{"Columns" -> {
            Offset[0.27999999999999997`], {
             Offset[0.]}, 
            Offset[0.27999999999999997`]}, "Rows" -> {
            Offset[0.2], {
             Offset[0.4]}, 
            Offset[0.2]}}],
       ButtonBar[{
        "Genera Esercizio" :> (UiComponents`Private`tarResult = 1; 
          UiComponents`Private`tcrResult = 0; While[
            Or[
            UiComponents`Private`tarResult > UiComponents`Private`tcrResult, 
             UiComponents`Private`tarResult == 
             UiComponents`Private`tcrResult], 
            UiComponents`Private`etpb = RandomInteger[{10, 30}]; 
            UiComponents`Private`atb = RandomInteger[{10, 30}]; 
            UiComponents`Private`dtb = RandomInteger[{0, 5}]; 
            UiComponents`Private`etpe = RandomInteger[{10, 30}]; 
            UiComponents`Private`ate = RandomInteger[{10, 30}]; 
            UiComponents`Private`dte = RandomInteger[{10, 30}]; 
            UiComponents`Private`dtb = RandomInteger[{0, 5}]; 
            UiComponents`Private`dte = RandomInteger[{10, 30}]; 
            UiComponents`Private`days = RandomInteger[{27, 30}]; 
            UiComponents`Private`calcResult; Null]; Null), 
         "Verifica Risultato" :> If[
           And[
            And[
             ValueQ[UiComponents`Private`etpb], 
             ValueQ[UiComponents`Private`atb], 
             ValueQ[UiComponents`Private`dtb], 
             ValueQ[UiComponents`Private`etpe], 
             ValueQ[UiComponents`Private`ate], 
             ValueQ[UiComponents`Private`dte], 
             ValueQ[UiComponents`Private`days], 
             ValueQ[UiComponents`Private`tcr], 
             ValueQ[UiComponents`Private`solution], 
             ValueQ[UiComponents`Private`tar], 
             ValueQ[UiComponents`Private`cte]], 
            Not[
             Or[
              TrueQ[UiComponents`Private`etpb == Null], 
              TrueQ[UiComponents`Private`atb == Null], 
              TrueQ[UiComponents`Private`dtb == Null], 
              TrueQ[UiComponents`Private`etpe == Null], 
              TrueQ[UiComponents`Private`ate == Null], 
              TrueQ[UiComponents`Private`dte == Null], 
              TrueQ[UiComponents`Private`days == Null], 
              TrueQ[UiComponents`Private`tar == Null], 
              TrueQ[UiComponents`Private`cte == Null], 
              TrueQ[UiComponents`Private`tcr == Null], 
              TrueQ[UiComponents`Private`solution == Null]]]], 
           UiComponents`Private`calcResult; If[
             And[
             UiComponents`Private`tar == UiComponents`Private`tarResult, 
              UiComponents`Private`tcr == UiComponents`Private`tcrResult, 
              UiComponents`Private`cte == UiComponents`Private`cteResult, 
              UiComponents`Private`solution == 
              UiComponents`Private`ecDateResult], 
             UiComponents`Private`systemResponse = 
             Style["La tua risposta e` corretta!", FontColor -> Green], 
             UiComponents`Private`systemResponse = 
             Style["La risposta e` errata. Riprova!", FontColor -> Red]], 
           UiComponents`Private`systemResponse = 
           Style["Uno dei campi di input e` vuoto", FontColor -> Red]], 
         "Mostra Soluzione" :> (If[
            And[
             And[
              ValueQ[UiComponents`Private`etpb], 
              ValueQ[UiComponents`Private`atb], 
              ValueQ[UiComponents`Private`dtb], 
              ValueQ[UiComponents`Private`etpe], 
              ValueQ[UiComponents`Private`ate], 
              ValueQ[UiComponents`Private`dte], 
              ValueQ[UiComponents`Private`days]], 
             Not[
              Or[
               TrueQ[UiComponents`Private`etpb == Null], 
               TrueQ[UiComponents`Private`atb == Null], 
               TrueQ[UiComponents`Private`dtb == Null], 
               TrueQ[UiComponents`Private`etpe == Null], 
               TrueQ[UiComponents`Private`ate == Null], 
               TrueQ[UiComponents`Private`dte == Null], 
               TrueQ[UiComponents`Private`days == Null]]]], 
            UiComponents`Private`calcResult; 
            UiComponents`Private`cte = UiComponents`Private`cteResult; 
            UiComponents`Private`tar = UiComponents`Private`tarResult; 
            UiComponents`Private`tcr = UiComponents`Private`tcrResult; 
            UiComponents`Private`solution = UiComponents`Private`ecDateResult; 
            Null, UiComponents`Private`systemResponse = 
            Style["Uno dei campi di input e` vuoto", FontColor -> Red]]; 
          Null), "Pulisci Soluzione" :> (Clear[UiComponents`Private`cte]; 
          Clear[UiComponents`Private`tar]; Clear[UiComponents`Private`tcr]; 
          Clear[UiComponents`Private`solution]; 
          UiComponents`Private`systemResponse = 
           Style["", FontColor -> Transparent])}, Appearance -> {Automatic}]],
       ""}
    },
    AutoDelete->False,
    GridBoxAlignment->{"Columns" -> {Left, Left}, "Rows" -> {Left, Left}},
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Grid"],
  DynamicModuleValues:>{}]], "Output",
 CellChangeTimes->{
  3.8298923504132853`*^9, 3.829895016863209*^9, {3.829895410527952*^9, 
   3.8298954183552237`*^9}, 3.829895854016087*^9, 3.82989602635392*^9, 
   3.829896930144082*^9, 3.82989707391413*^9, 3.829897130483028*^9, 
   3.829897263450996*^9, 3.829897340836809*^9, 3.829897461817507*^9, 
   3.829897687808668*^9, 3.82989795473228*^9, {3.829898294707848*^9, 
   3.829898324258157*^9}, 3.829898394477325*^9, {3.829898502478671*^9, 
   3.829898569270274*^9}, 3.829899031674222*^9, 3.829899100130961*^9, {
   3.829899145295702*^9, 3.829899165535693*^9}, 3.8298994033074493`*^9, 
   3.829899816954916*^9, 3.829971756022354*^9, {3.8299734773955927`*^9, 
   3.829973483456764*^9}, 3.829973615093258*^9, 3.829973699717595*^9, {
   3.8299737723499517`*^9, 3.829973795539959*^9}, 3.829973870173757*^9, 
   3.829973950707201*^9, 3.8299740050321007`*^9, 3.8299740571772633`*^9, 
   3.829974987723942*^9, 3.829975544572242*^9, 3.8299762095738487`*^9, 
   3.829976418686965*^9, 3.829976612315061*^9, 3.829977202846444*^9, 
   3.829977348930951*^9, 3.829977394167416*^9, 3.829977655032209*^9, 
   3.8299778342362223`*^9, 3.829978107982366*^9, 3.829978904909452*^9, 
   3.829979030889497*^9, 3.82997913986549*^9, 3.8299793683155727`*^9, 
   3.829979542755479*^9, 3.829980039009027*^9, 3.829980124561652*^9, 
   3.8299802015424147`*^9, 3.8300553498221607`*^9, 3.830055441731049*^9, {
   3.830055482294793*^9, 3.830055485678234*^9}, {3.830055715057699*^9, 
   3.830055743647492*^9}, 3.830055968679885*^9, 3.8300564674363403`*^9, 
   3.830056947674047*^9, 3.830057003991436*^9, {3.830057075477263*^9, 
   3.830057102073003*^9}, 3.830057367932455*^9, 3.830057431380917*^9, 
   3.8300578016077833`*^9, 3.8300579718021584`*^9, 3.830227068981781*^9, 
   3.830227243231667*^9, 3.830227323804008*^9, 3.830227477316125*^9},
 CellLabel->"Out[9]=",ExpressionUUID->"dedc4c99-be0c-4640-bdec-63d728bbb7cb"]
}, Open  ]]
},
WindowSize->{1848, 1016},
WindowMargins->{{Automatic, 0}, {0, Automatic}},
TaggingRules->{
 "WelcomeScreenSettings" -> {"FEStarting" -> False}, "TryRealOnly" -> False},
FrontEndVersion->"12.2 for Linux x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"266c9a4d-37d6-4584-8907-6c40d9e2832c"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 413, 7, 30, "Input",ExpressionUUID->"c44f47db-f7c4-455a-9dd1-e84dd032c2c4"],
Cell[974, 29, 1014, 22, 113, "Input",ExpressionUUID->"4a9f226c-143e-42c8-8613-180a147a3e9e",
 InitializationCell->True],
Cell[1991, 53, 152, 3, 30, "Input",ExpressionUUID->"fb3adc08-6d92-4494-b297-79ebc8875880"],
Cell[CellGroupData[{
Cell[2168, 60, 185, 2, 30, "Input",ExpressionUUID->"fd6c733d-0a84-4210-8cf9-ea9d17e23a3b"],
Cell[2356, 64, 6584, 146, 145, "Output",ExpressionUUID->"11c389b5-a649-47fd-b4d0-62928cc3aab7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8977, 215, 799, 11, 36, "Code",ExpressionUUID->"5c6a4752-8dcf-4890-ac63-c0dab3d15098",
 InitializationCell->False],
Cell[9779, 228, 14445, 283, 369, "Output",ExpressionUUID->"dedc4c99-be0c-4640-bdec-63d728bbb7cb"]
}, Open  ]]
}
]
*)

