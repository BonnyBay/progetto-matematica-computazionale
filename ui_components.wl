(* ::Package:: *)

BeginPackage["UiComponents`"]

wipEsUniversale::usage = "Parte di frontend per il calcolo del Work in Progress Limit."
expectedCompletionDateEsUniversale::usage = "Parte di frontend per il calcolo della data prevista di completamento del progetto."

Begin["`Private`"]

AppendTo[$Path, NotebookDirectory[]];
<<pm_library.wl;

wipEsUniversale :=
	DynamicModule[{(*armp, psc, solution, systemResponse1, wipResult*)}, (*Possibile passare le variabili?*)
		calcResultEs1 :=
			Module[{},
				wipResult = StringRiffle[wipLimit[ToExpression[StringSplit[armp, ","]], psc], ","];
			];
		Grid[{
			{"Numero medio di task conclusi per ogni persona in un mese:", InputField[Dynamic[armp], String, FieldHint -> "6,2,3"]},
			{"Numero di persone assegnate alla colonna che e` andata piu` lenta:", InputField[Dynamic[psc], Number, FieldHint -> "3"]},
			{"Risultato:", InputField[Dynamic[solution], String, FieldHint -> "2, 5, 3"]},
			{Style[Dynamic[systemResponse1], FontColor -> Transparent]},
			{ButtonBar[{
				"Genera Esercizio" :> (
					armp = StringRiffle[Table[RandomInteger[{1, 10}], {3}], ","];
					psc = RandomInteger[{1, 5}];
				),
				(*Check che le variabili connesse alle input box non siano undefined e nulle*)
				"Verifica Risultato" :> (If[(ValueQ[armp] && ValueQ[psc] && ValueQ[solution]) && 
				(Not[TrueQ[armp == ""] || TrueQ[psc == Null] || TrueQ[solution == ""]]) && 
				(AllTrue[Quiet[ToExpression[StringSplit[armp,","]]],IntegerQ] && IntegerQ[psc] && 
				AllTrue[Quiet[ToExpression[StringSplit[solution,","]]],IntegerQ]),
					(*Then*)
					calcResultEs1;
					If[Equal[StringDelete[solution, " "], wipResult],
						(*Then*)
						systemResponse1 = Style["La tua risposta e` corretta!", FontColor -> Green]
						,
						(*Else*)
						systemResponse1 = Style["La risposta e` errata. Riprova!", FontColor -> Red]
					]
					,
					(*Else*)
					systemResponse1 = Style["Uno dei campi di input presenta degli errori. Controlla di aver riempito gli spazi necessari e che gli input non contengano lettere o numeri con virgole al posto di interi.", FontColor -> Red]
				]),
				"Mostra Soluzione" :> (If[(ValueQ[armp] && ValueQ[psc]) && 
				(Not[TrueQ[armp == ""] || TrueQ[psc == Null]]) && 
				(AllTrue[Quiet[ToExpression[StringSplit[armp,","]]],IntegerQ] && IntegerQ[psc]),
					calcResultEs1; 
					solution = wipResult;
					,
					systemResponse1 = Style["Uno dei campi di input presenta degli errori. Controlla di aver riempito gli spazi necessari e che gli input non contengano lettere o numeri con virgole al posto di interi.", FontColor -> Red]
				]),
				"Pulisci Soluzione" :> (Clear[solution]; systemResponse1 = Style["", FontColor -> Transparent])
			}]}
		}, Alignment -> {{Left, Left}, {Left, Left}, {Left, Left}, {Left}, {Left}}]
	];

expectedCompletionDateEsUniversale :=
	DynamicModule[{ (*etpb, atb, dtb, etpe, ate, dte, days, tcr, tar, cte, solution, systemResponse2,
            totalTaskBeginning, totalTaskEnd, tarResult, tcrResult, cteResult, ecDateResult*)
		},
		calcResultEs2 :=
			Module[{},
				totalTaskBeginning = etpb + atb + dtb;
				totalTaskEnd = etpe + ate + dte;
				cteResult = currentTaskEstimate[etpe, ate];
				tarResult = ToExpression[ToString[DecimalForm[N[taskAddRate[totalTaskBeginning, totalTaskEnd, days]], {Infinity, 2}]]];
				tcrResult = ToExpression[ToString[DecimalForm[N[taskCompletionRate[dtb, dte, days]], {Infinity, 2}]]];
				ecDateResult = expectedCompletionDate[cteResult, tcrResult, tarResult];
			];
		Grid[{
			{"Stima dei task in sospeso a inizio mese:", InputField[Dynamic[etpb], Number, FieldHint -> "74"]},
			{"Task attivi a inizio mese:", InputField[Dynamic[atb], Number, FieldHint -> "15"]},
			{"Task completati a inizio mese:", InputField[Dynamic[dtb], Number, FieldHint -> "2"]},
			{"Stima dei task in sospeso a fine mese:", InputField[Dynamic[etpe], Number, FieldHint -> "55"]},
			{"Task ancora attivi a fine mese:", InputField[Dynamic[ate], Number, FieldHint -> "16"]},
			{"Task completati a fine mese:", InputField[Dynamic[dte], Number, FieldHint -> "25"]},
			{"Giorni nel mese:", InputField[Dynamic[days], Number, FieldHint -> "29"]},
			{"Task completion rate:", InputField[Dynamic[tcr], String, FieldHint -> "0.79"]},
			{"Task add rate:", InputField[Dynamic[tar], String, FieldHint -> "0.17"]},
			{"Current task estimate:", InputField[Dynamic[cte], Number, FieldHint -> "71"]},
			{"Giorni rimanenti al completamento:", InputField[Dynamic[solution], Number, FieldHint -> "114"]},
			{Style[Dynamic[systemResponse2], FontColor -> Transparent]},
			{ButtonBar[{
				"Genera Esercizio" :> (
					(*Caso artificioso per entrare nel while. "Simuliamo" una sorta 
                    di do while*)
					tarResult = 1;
					tcrResult = 0;
					(*Check con precalcolo per evitare che tar sia > di tcr, perche in questo caso 
					il numero di giorni diventerebbe negativo. Check che non siano uguali altrimenti ho
					una divisione per 0.*)
					While[(tarResult > tcrResult || tarResult == tcrResult),
						etpb = RandomInteger[{10, 30}];
						atb = RandomInteger[{10, 30}];
						dtb = RandomInteger[{0, 5}];
						etpe = RandomInteger[{10, 30}];
						ate = RandomInteger[{10, 30}];
						dte = RandomInteger[{10, 30}];
						dtb = RandomInteger[{0, 5}];
						dte = RandomInteger[{10, 30}];
						(*Il primo giorno non viene contato, viene fatto -1. Questo ha un impatto sul dtb
                        perche` questo valore e` pari al numero di task svolti il primo gg.*)
						days = RandomInteger[{27, 30}];
						(*Non fa il throw dell'errore in caso di divisione per 0*)
						Quiet[calcResultEs2];
					];
				),
				(*Check che le variabili connesse alle input box non siano undefined e nulle*)
				"Verifica Risultato" :> (If[(ValueQ[etpb] && ValueQ[atb] && ValueQ[dtb] && ValueQ[etpe] && ValueQ[ate] && ValueQ[dte]
				    && ValueQ[days] && ValueQ[tcr] && ValueQ[solution] && ValueQ[tar] && ValueQ[cte])
					&& (Not[TrueQ[etpb == Null] || TrueQ[atb == Null] || TrueQ[dtb == Null] || TrueQ[etpe == Null]
					|| TrueQ[ate == Null] || TrueQ[dte == Null] || TrueQ[days == Null] || TrueQ[tar == Null]
					|| TrueQ[cte == Null] || TrueQ[tcr == Null] || TrueQ[solution == Null]]) && (IntegerQ[etpb] && IntegerQ[atb]
					&& IntegerQ[dtb] && IntegerQ[etpe] && IntegerQ[ate] && IntegerQ[dte] && IntegerQ[days] && IntegerQ[cte] 
					&& IntegerQ[solution] && Quiet[NumberQ[ToExpression[tcr]]] && Quiet[NumberQ[ToExpression[tar]]]),
					(*Then*)
					calcResultEs2;
					If[Equal[ToExpression[tar], tarResult] && Equal[ToExpression[tcr], tcrResult] && Equal[cte, cteResult] && Equal[solution, ecDateResult],
						(*Then*)
						systemResponse2 = Style["La tua risposta e` corretta!", FontColor -> Green]
						,
						(*Else*)
						systemResponse2 = Style["La risposta e` errata. Riprova!", FontColor -> Red]
					]
					,
					(*Else*)
					systemResponse2 = Style["Uno dei campi di input presenta degli errori. Controlla di aver riempito gli spazi necessari e che gli input non contengano lettere o numeri con virgole al posto di interi.", FontColor -> Red]
				]),
				"Mostra Soluzione" :> (If[(ValueQ[etpb] && ValueQ[atb] && ValueQ[dtb] && ValueQ[etpe] && ValueQ[ate] && ValueQ[dte]
				    && ValueQ[days]) && (Not[TrueQ[etpb == Null] || TrueQ[atb == Null] || TrueQ[dtb == Null] || TrueQ[etpe == Null]
					|| TrueQ[ate == Null] || TrueQ[dte == Null] || TrueQ[days == Null]]) && (IntegerQ[etpb] && IntegerQ[atb]
					&& IntegerQ[dtb] && IntegerQ[etpe] && IntegerQ[ate] && IntegerQ[dte] && IntegerQ[days]),
					    (*Then*)
						calcResultEs2;
						cte = cteResult;
						tar = ToString[tarResult];
						tcr = ToString[tcrResult];
						solution = ecDateResult;
						,
						(*Else*)
						systemResponse2 = Style["Uno dei campi di input presenta degli errori. Controlla di aver riempito gli spazi necessari e che gli input non contengano lettere o numeri con virgole al posto di interi.", FontColor -> Red],
					];
				),
				"Pulisci Soluzione" :> (Clear[cte];
					Clear[tar];
					Clear[tcr];
					Clear[solution];
					systemResponse2 = Style["", FontColor -> Transparent]
				)
			}]}
		}, Alignment -> {{Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left, Left}, {Left}, {Left}}]
	];

End[]
EndPackage[]
