(* ::Package:: *)

BeginPackage["PmUtils`"]

wipLimit::usage = "wipLimit[armp, psc] The function calculates the work in progress limit for kanban. 
The parameters required are two: a list containing the average number of tasks completed in a month
by each person for each column and the number of people who worked on the column that went slower."
taskAddRate::usage = "taskAddRate[ntb_Integer, nte_Integer, dm_Integer] The function measures change
in plans. The required parameters are three: total (pending + active + done task) number of tasks 
at the beginning of the month (ntb) and the total number of tasks at the end of the month (nte). The
last parameter is the number of working days in the month (dm). This function returns the task added
(positive number) or cutted per day (negative number)."
taskCompletionRate::usage = "taskCompletionRate[ntb_Integer, nte_Integer,dm_Integer] This function
calculates the number of completed tasks per day during a range of time. Required parameters are three:
number of done task at the end and at the beginning of the month and the number of working days."
currentTaskEstimate::usage = "currentTaskEstimate[etp_integer, at_integer] This function requires two 
parameters: the number of active and pending task at the end of the month. Current task estimate
represents remaining work."
expectedCompletionDate::usage = "expectedCompletionDate[cte_Integer, tcr_Double, tar_Double] The function
calculates the number of days until the team will complete a given set of task. The required parameters
are three: current task estimate (CTE), task completion rate (TCR) and task add rate (TAR)."

Begin["`Private`"]

(*armp = average rate per month per person*)
(*psc = people assigned to slowest column*)
wipLimit[armp_List, psc_Integer] := 
	Module[{slowestRate, columnThroughput, outputList, buffer},
		(*Si ottiene il numero di task della colonna che e` andata piu` lenta*)
		slowestRate = Min[armp];
		(*Il throughput della colonna e` uguale al numero di task medi per persona 
			moltiplicato per il numero di persone che lavorano a quella colonna*)
		columnThroughput = slowestRate * psc;
		(*Dividere il throughput della colonna piu` lenta per il numero di task medi per
		 persona di ogni colonna, restituisce il numero di persone necessarie per ogni colonna
		 a sostenere il throughput della colonna piu` lenta*)
		outputList = columnThroughput / armp;
		(*Si aggiunge il 50% per avere margine e si arrotonda per eccesso. Brechner suggerisce questo valore*)
		buffer = 1.5;
		outputList = Ceiling[outputList * buffer]; 
		Return[outputList];
	];

(*ntb = (total) number of tasks at the beginning of the month*)
(*nte = (total) number of tasks at the end of the month*)
(*dm = working days in the month*)	
taskAddRate[ntb_Integer, nte_Integer, dm_Integer] :=
	Module[{taskAddRate},
		(*Il numero di task aggiunti per ogni giorno del mese e` dato dalla
		differenza tra i task all'inizio del mese, sottratti a quelli alla fine del mese e 
		diviso per il numero di giorni del mese.
		Calcola i task aggiunti alla board per ogni giorno.*)
		taskAddRate = (nte - ntb)/dm;
		Return[taskAddRate];
	];

(*ntb =  number of (done) tasks at the beginning of the month*)
(*nte =  number of (done) tasks at the end of the month*)
(*dm = working days in the month*)	
taskCompletionRate[ntb_Integer, nte_Integer,dm_Integer] := 
	Module[{taskCompletionRate},
		(*La media di task completati per ogni giorno*)
		taskCompletionRate = (nte - ntb)/dm;
		Return[taskCompletionRate];
	];
		
(*etpe = estimate task pending at end *)
(*ate = active task at the end of the month*)
currentTaskEstimate[etpe_Integer, ate_Integer] :=
	Module[{currentTaskEstimate},
		(*Rappresenta il totale dei task da svolgere.*)
		currentTaskEstimate = etpe + ate;
		Return[currentTaskEstimate];
	];
		
expectedCompletionDate[cte_Integer, tcr_Real, tar_Real] :=
	Module[{remainingDays},
		(*Task rimanenti diviso i task completati per ogni giorno meno i task 
		che si aggiungono per ogni giorno.*)
		remainingDays = Ceiling[cte/(tcr-tar)];
		Return[remainingDays];
	];
		
End[]
EndPackage[]















